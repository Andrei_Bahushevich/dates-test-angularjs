import { Component, OnInit } from 'angular-ts-decorators';

@Component({
  selector: '$selectorPrefix$$kebabCaseName$',
  template: require('./$name$.html'),
  styles: [require('./$name$.less')],
})
export class $PascalCaseName$Component implements OnInit {

  /*@ngInject*/
  constructor() { }

  ngOnInit() { }
}
