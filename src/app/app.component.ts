import { Component } from 'angular-ts-decorators';
import { IMcDatesChangeEvent } from './mc-dates/mc-dates.component';

@Component({
  selector: 'app-root',
  template: require('./app.component.html'),
  styles: [require('./app.component.less')]
})
export class AppComponent {
  title = 'Tour of Heroes';

  date1 = null;
  date2 = null;

  resetDates() {
    this.date1 = null;
    this.date2 = null;
  }

  onDatesChange($event: IMcDatesChangeEvent) {
    alert(`${$event.dateFrom} - ${$event.dateTo}`);
  }

}
