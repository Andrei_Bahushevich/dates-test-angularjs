import 'angular-material';
import 'angular-material/angular-material.css';
import { NgModule } from 'angular-ts-decorators';
import * as moment from 'moment';
import { AppComponent } from './app.component';
import { IsoToDatePipe } from './iso-to-date.pipe';
import { McDatesComponent } from './mc-dates/mc-dates.component';
import './styles.css';

const dateFormat = 'DD.MM.YYYY';

@NgModule({
  id: 'AppModule',
  imports: [
    'ngMaterial'
  ],
  declarations: [
    AppComponent,
    McDatesComponent,
    IsoToDatePipe,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {


  /*@ngInject*/
  static config($mdDateLocaleProvider) {
    $mdDateLocaleProvider.parseDate = dateString => {
      const m = moment(dateString, dateFormat, true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $mdDateLocaleProvider.formatDate = date => {
      const m = moment(date);
      return m.isValid() ? m.format(dateFormat) : '';
    };

    // Allow only a day and month to be specified.
    // This is required if using the 'M/D' format with moment.js.
    $mdDateLocaleProvider.isDateComplete = dateString => {
      dateString = dateString.trim();

      // Look for two chunks of content (either numbers or text) separated by delimiters.
      const re = /^(([a-zA-Z]{3,}|[0-9]{1,4})([ .,]+|[/-]))([a-zA-Z]{3,}|[0-9]{1,4})/;
      return re.test(dateString);
    };

    $mdDateLocaleProvider.firstDayOfWeek = 1;
  }

}
