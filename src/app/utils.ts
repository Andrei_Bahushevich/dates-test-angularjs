import * as moment from 'moment';

export const utils = {

  getISOFromString(str: string) {
    return moment(str, 'YYYY-MM-DD').toISOString();
  },

  getStringFromISO(str: string) {
    return str ? moment(str).format('YYYY-MM-DD') : str;
  },

  setObjectValueByPath(object: { [key: string]: any }, path: string[], value: any) {
    const lastKey = path[path.length - 1];

    path.slice(0, path.length - 1).forEach(key => {
      object = object[key];
    });

    object[lastKey] = value;

  }
};

