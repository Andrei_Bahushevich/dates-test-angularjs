import { Pipe, PipeTransform } from 'angular-ts-decorators';

@Pipe({ name: 'isoToDate' })
export class IsoToDatePipe implements PipeTransform {

  private cache = new Map<string, Date>();

  transform(iso: string) {
    if (!iso) {
      return null;
    }

    if (!this.cache.has(iso)) {
      this.cache.set(iso, new Date(iso));
    }

    return this.cache.get(iso);
  }
}
