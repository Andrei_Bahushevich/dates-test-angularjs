import { Component, Input, Output } from 'angular-ts-decorators';
import * as moment from 'moment';

import { utils } from '../utils';

@Component({
  selector: 'mc-dates',
  template: require('./mc-dates.component.html'),
  styles: [require('./mc-dates.component.less')],
})
export class McDatesComponent {

  private _dateFrom: string;
  private _dateTo: string;

  @Input('=')
  get dateFrom() {
    return this._dateFrom;
  }
  set dateFrom(val: string) {
    this._dateFrom = val;
    this.isoDateFrom = utils.getISOFromString(val);
  }

  @Input('=')
  get dateTo() {
    return this._dateTo;
  }
  set dateTo(val: string) {
    this._dateTo = val;
    this.isoDateTo = utils.getISOFromString(val);
  }

  isoDateFrom: string;
  isoDateTo: string;

  @Output() mcChange: ({ $event: IMcDatesChangeEvent }) => void;

  setToday() {
    this.isoDateFrom = moment().startOf('day').toISOString();
    this.isoDateTo = moment().endOf('day').toISOString();
    this.onDatesChanged();
  }

  setYesterday() {
    this.isoDateFrom = moment().subtract(1, 'd').startOf('day').toISOString();
    this.isoDateTo = moment().subtract(1, 'd').endOf('day').toISOString();
    this.onDatesChanged();
  }

  set2WeeksAgo() {
    this.isoDateFrom = moment().subtract(14, 'd').toISOString();
    this.isoDateTo = moment().toISOString();
    this.onDatesChanged();
  }

  setMonthAgo() {
    this.isoDateFrom = moment().subtract(30, 'd').toISOString();
    this.isoDateTo = moment().toISOString();
    this.onDatesChanged();
  }

  resetDates() {
    this.isoDateFrom = null;
    this.isoDateTo = null;
    this.onDatesChanged();
  }


  onDatesChanged() {
    const $event = this.getChangeEvent();
    const isNotChanged = $event.dateFrom === this.dateFrom && $event.dateTo === this.dateTo;

    if (isNotChanged) {
      return;
    }

    this.onDateFromChanged();
    this.onDateToChanged();
    this.mcChange({
      $event
    });
  }

  private onDateFromChanged() {
    const value = utils.getStringFromISO(this.isoDateFrom);
    this.dateFrom = value;
  }

  private onDateToChanged() {
    const value = utils.getStringFromISO(this.isoDateTo);
    this.dateTo = value;
  }

  private getChangeEvent(): IMcDatesChangeEvent {
    return {
      dateFrom: utils.getStringFromISO(this.isoDateFrom),
      dateTo: utils.getStringFromISO(this.isoDateTo)
    };
  }


}

export interface IMcDatesChangeEvent {
  dateFrom: string;
  dateTo: string;
}
