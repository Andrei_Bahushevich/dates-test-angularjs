import { ComponentFixture, DebugElement, TestBed } from 'angularjs-testbed';
import { McDatesComponent } from './mc-dates.component';

describe('McDatesComponent', () => {
  let component: McDatesComponent;
  let fixture: ComponentFixture<McDatesComponent>;
  let debugElement: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [McDatesComponent]
    });

    fixture = TestBed.createComponent(McDatesComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  afterEach(() => TestBed.resetTestingModule());

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
